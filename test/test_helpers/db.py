""" ORM для доступа к БД."""

from sqlalchemy.sql import expression
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Boolean, TEXT

__all__ = ['User', 'Account', 'Image', 'Project', 'Permission', 'ProjectRule']

Db = declarative_base()


class User(Db):
    __tablename__ = 'User'

    Id = Column('@User', Integer, primary_key=True)
    Login = Column('Login', String(100), nullable=False)
    Password = Column('Password', String(50), nullable=False)
    IsSuperuser = Column('IsSuperuser', Boolean, nullable=False, default=expression.false())
    Disabled = Column('Disabled', Boolean, nullable=False, default=expression.false())

    def __repr__(self):
        return f"<User(id={self.Id} login='{self.Login}', " \
               f"is_superuser='{self.IsSuperuser}', disabled='{self.Disabled}')>"


class Account(Db):
    __tablename__ = 'Account'

    Id = Column('@Account', Integer, primary_key=True)
    FirstName = Column('FirstName', String(300), nullable=False)
    LastName = Column('LastName', String(300), nullable=False)
    UserId = Column('UserId', Integer, ForeignKey("User.@User"))
    Status = Column('Status', String(300), nullable=False)
    Create = Column('Create', DateTime(timezone=True))
    Image = Column('Image', Integer, ForeignKey("Image.@Image"))
    Description = Column('Description', TEXT, nullable=False)
    LongDescription = Column('LongDescription', TEXT, nullable=False)

    def __repr__(self):
        return f"<Account(id={self.Id} first_name='{self.FirstName}', last_name='{self.LastName}'" \
               f", user_id={self.UserId}, status='{self.Status}', create='{self.Create}', image='{self.Image}'" \
               f", description='{self.Description}', long_description='{self.LongDescription}')>"


class Image(Db):
    __tablename__ = 'Image'

    Id = Column('@Image', Integer, primary_key=True)
    Title = Column('Title', String(300), nullable=False)
    Link = Column('Link', TEXT, nullable=False)
    ImageLink = Column('ImageLink', TEXT, nullable=False)
    Description = Column('Description', TEXT)
    UserId = Column('UserId', Integer, ForeignKey("User.@User"))
    Create = Column('Create', DateTime(timezone=True))
    

class Project(Db):
    __tablename__ = 'Project'

    Id = Column('@Project', Integer, primary_key=True)
    Name = Column('Name', String(300), nullable=False)
    Description = Column('Description', TEXT, nullable=False)
    LongDescription = Column('LongDescription', TEXT, nullable=False)
    Date = Column('Date', DateTime, nullable=False)
    Repository = Column('Repository', TEXT, nullable=False)
    Image = Column('Image', Integer, ForeignKey("Image.@Image"))
    UserId = Column('UserId', Integer, ForeignKey("User.@User"))
    Create = Column('Create', DateTime(timezone=True))
    

class Permission(Db):
    __tablename__ = 'Permission'

    Id = Column('@Permission', Integer, primary_key=True)
    Name = Column('Name', String(300), nullable=False)
    UserId = Column('UserId', Integer, ForeignKey("User.@User"))
    Create = Column('Create', DateTime(timezone=True))


class ProjectRule(Db):
    __tablename__ = 'ProjectRule'

    Id = Column('@ProjectRule', Integer, primary_key=True)
    ProjectId = Column('ProjectId', Integer, ForeignKey("Project.@Project"))
    AccountId = Column('AccountId', Integer, ForeignKey("Account.@Account"))
    Rule = Column('Rule', String(300))
    Create = Column('Create', DateTime(timezone=True))

