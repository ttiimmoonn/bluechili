""" Вспомогательный класс для тестирования."""

import json

from passlib.hash import sha256_crypt

from sqlalchemy import delete, insert
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.asyncio import AsyncSession

from aiohttp.test_utils import AioHTTPTestCase

from test.test_helpers import db

from bluechili.bl.auth.login import Login
from bluechili.core.model.user import User
from bluechili.core.setting import Settings
from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic
from bluechili.core.web.init_application import init_application
from bluechili.core.web.web_response import default_serializable_json


class CustomAioHTTPTestCase(AioHTTPTestCase):
    """ Расширенный класс для тестирования приложения с БД."""
    # Указатель на БД
    Db: AsyncSession

    _PASSWORD_HASH = {
        "password": sha256_crypt.hash('password'),
        "test": sha256_crypt.hash('test'),
    }

    async def execute(self, query: [str, ], params=None, **kwargs):
        """ Выполненить sql запрос."""

        def get_result(q_result):
            if not q_result:
                return None

            if len(q_result) == 1:
                if len(q_result[0]) == 1:
                    return q_result[0][0]
                return q_result[0]
            return q_result

        if params is None:
            params = dict()

        params.update(kwargs)

        if isinstance(query, str):
            async with self.Db.cursor() as cursor:
                await cursor.execute(query, params)
                try:
                    result = await cursor.fetchall()
                except Exception:
                    return None
                return get_result(result)

        _query = query.compile(dialect=postgresql.dialect())
        params.update(_query.params)

        async with self.Db.cursor() as cursor:
            await cursor.execute(str(_query), params)
            try:
                result = await cursor.fetchall()
            except Exception:
                return None
            return get_result(result)

    async def clear_db(self):
        """ Очистить базы данных."""
        await self.execute(delete(db.User))
        await self.execute(delete(db.Account))
        await self.execute(delete(db.Image))
        await self.execute(delete(db.Project))
        await self.execute(delete(db.Permission))
        await self.execute(delete(db.ProjectRule))

    async def setUpAsync(self):
        """ Подготовка к тестам."""
        self.Db = self.app.db

        await self.execute("BEGIN;")
        await self.execute("SAVEPOINT unittest;")

        await self.clear_db()

    async def tearDownAsync(self):
        """ Отчистка мета данных после тестов."""
        await self.execute("ROLLBACK TO SAVEPOINT unittest;")
        await self.execute("COMMIT;")

    async def get_application(self):
        """ Получить приложение, которое будет тестироваться."""
        return await init_application(Settings())

    async def invoke(self, object_bl: [BusinessLogic], params: dict = None, **kwargs):
        """ Вызвать метод бизнес логики.

        :param object_bl: Бизнес логика.
        :param params: Параметры для вызова.
        :param kwargs: Параметры для вызова в формате именованных аргументов.
        """

        result_params = dict()

        if params is not None:
            result_params.update(params)

        if kwargs:
            result_params.update(kwargs)

        result_data = json.dumps(result_params, default=default_serializable_json)
        headers = {'content-type': 'application/json'}

        return await self.client.request(
            object_bl.METHOD,
            object_bl.ROUTE,
            data=result_data,
            headers=headers
        )


def create_user(
        login: str = 'test',
        password: str = 'test',
        is_super: bool = True,
        permissions=None,
        auth: bool = True
):
    """
    Декоратор для создания пользователя перед тестом.
    Создает пользователя и возвращает User.

    :param login: Логин пользователя
    :param password: Пароль пользователя
    :param is_super: Является ли пользователь супером
    :param permissions: Список разрешений, которые нужно выдать пользователю
    :param auth: Нужно ли авторизовывать пользователя
    """
    def decorator(func):
        async def wrapper(self, *args, **kwargs):
            user_id = await self.execute(
                insert(db.User)
                .values(
                    Login=login,
                    Password=self._PASSWORD_HASH.get(
                        password,
                        sha256_crypt.hash(password)
                    ),
                    IsSuperuser=is_super
                )
                .returning(db.User.Id)
            )
            if not is_super and permissions is not None and isinstance(permissions, list):
                for permission in permissions:
                    if permission not in list(Permission.__dict__.values()):
                        continue
                    await self.execute(
                        insert(db.Permission)
                        .values(Name=permission, UserId=user_id)
                    )

            if auth:
                await self.invoke(Login, Login=login, Password=password)
            return await func(self, User(user_id, login, password, is_super), *args, **kwargs)
        return wrapper
    return decorator
