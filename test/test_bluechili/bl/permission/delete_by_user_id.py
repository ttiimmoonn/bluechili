from sqlalchemy import select

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.permission.delete_by_user_id import DeleteByUserId

from bluechili.core.account.permission import Permission

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestDeleteByUserId(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(is_super=True, permissions=[Permission.USER_EDIT])
    async def test_1(self, user):
        """ Удаляем права у пользователя."""
        response = await self.invoke(DeleteByUserId, UserId=user.Id, PermissionName=Permission.USER_EDIT)

        self.assertEqual(response.status, web.HTTPOk.status_code)

        result = await self.execute(select(db.Permission))

        self.assertIsNone(result)

    @unittest_run_loop
    @create_user(is_super=False, permissions=[Permission.USER_EDIT])
    async def test_2(self, user):
        """ Пытаемся удалить права для пользователя, но у пользователя нет такого разрешения."""
        response = await self.invoke(DeleteByUserId, UserId=user.Id, PermissionName=Permission.USER_EDIT)

        self.assertEqual(response.status, web.HTTPForbidden.status_code)
