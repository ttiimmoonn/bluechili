from sqlalchemy import select

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.permission.set_by_user_id import SetByUserId

from bluechili.core.account.permission import Permission

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestSetByUserId(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_1(self, user):
        """ Добавляем права для пользователя."""
        response = await self.invoke(SetByUserId, UserId=user.Id, PermissionName=Permission.USER_EDIT)

        self.assertEqual(response.status, web.HTTPOk.status_code)

        perm_id, perm_name, perm_user_id, perm_create = await self.execute(select(db.Permission))

        self.assertEqual(user.Id, perm_user_id)
        self.assertIsNotNone(perm_create)
        self.assertTrue(isinstance(perm_id, int))
        self.assertEqual(perm_name, Permission.USER_EDIT)

    @unittest_run_loop
    @create_user(is_super=False)
    async def test_2(self, user):
        """ Пытаемся добавить права для пользователя, но у пользователя нет такого разрешения."""
        response = await self.invoke(SetByUserId, UserId=user.Id, PermissionName=Permission.USER_EDIT)

        self.assertEqual(response.status, web.HTTPForbidden.status_code)

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_3(self, user):
        """ Пытаемся добавить права для пользователя, которые не заложены системой."""
        response = await self.invoke(SetByUserId, UserId=user.Id, PermissionName='fake permission')

        self.assertEqual(response.status, web.HTTPBadRequest.status_code)
