from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.core.account.permission import Permission
from bluechili.bl.permission.get_by_user_id import GetByUserId

from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestGetByUserId(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(
        is_super=False,
        permissions=[Permission.PERMISSION_EDIT, Permission.USER_EDIT, Permission.PROJECT_EDIT]
    )
    async def test_1(self, user):
        """ Получить права для пользователя."""
        response = await self.invoke(GetByUserId, UserId=user.Id)

        self.assertEqual(response.status, web.HTTPOk.status_code)

        param = await response.json()
        for item, rule in enumerate([Permission.PERMISSION_EDIT, Permission.PROJECT_EDIT, Permission.USER_EDIT]):
            self.assertEqual(param[item]['UserId'], user.Id)
            self.assertEqual(param[item]['Name'], rule)
            self.assertTrue(param[item]['Create'])

    @unittest_run_loop
    @create_user(is_super=False)
    async def test_2(self, user):
        """ Пытаемся получить права для пользователя, но у пользователя нет такого разрешения."""
        response = await self.invoke(GetByUserId, UserId=user.Id)
        self.assertEqual(response.status, web.HTTPForbidden.status_code)
