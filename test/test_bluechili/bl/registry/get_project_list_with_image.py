from datetime import datetime

from sqlalchemy import insert

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.core.model.project import ProjectWithImage
from bluechili.bl.registry.get_project_list_with_image import GetProjectListWithImage

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestGetProjectListWithImage(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_1(self, user):
        """ Поучить все проекты. Проверяем поля в ответе."""
        image_id = await self.execute(
            insert(db.Image)
            .values(Title='Title', Link='Link', ImageLink='ImageLink', Description='ImageDescription', UserId=user.Id)
            .returning(db.Image.Id)
        )

        project_1 = ProjectWithImage(
            None, 'Name', 'Description', datetime.now(), 'Repository', 'LongDescription',
            user.Id, None, image_id, "Title", "Link", "ImageLink", "ImageDescription"
        )
        project_2 = ProjectWithImage(
            None, 'Name1', 'Description1', datetime.now(), 'Repository', 'LongDescription',
            user.Id, None, image_id, "Title", "Link", "ImageLink", "ImageDescription"
        )

        for project in [project_1, project_2]:
            project.Id = await self.execute(
                insert(db.Project)
                .values(
                    Name=project.Name, Description=project.Description, LongDescription=project.LongDescription,
                    Date=project.Date, Repository=project.Repository, Image=project.Image, UserId=project.UserId
                )
                .returning(db.Project.Id)
            )
        response = await self.invoke(
            GetProjectListWithImage,
            Page=None,
            Limit=None
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
        date_response = await response.json()

        for index, project in enumerate([project_2, project_1]):
            self.assertEqual(date_response[index]['Id'], project.Id)
            self.assertEqual(date_response[index]['Name'], project.Name)
            self.assertEqual(date_response[index]['Description'], project.Description)
            self.assertIsNotNone(date_response[index]['Date'])
            self.assertEqual(date_response[index]['Repository'], project.Repository)
            self.assertEqual(date_response[index]['LongDescription'], project.LongDescription)
            self.assertEqual(date_response[index]['UserId'], project.UserId)
            self.assertIsNotNone(date_response[index]['Create'])
            self.assertEqual(date_response[index]['UserId'], project.UserId)
            self.assertEqual(date_response[index]['Image'], project.Image)
            self.assertEqual(date_response[index]['Title'], project.Title)
            self.assertEqual(date_response[index]['Link'], project.Link)
            self.assertEqual(date_response[index]['ImageLink'], project.ImageLink)
            self.assertEqual(date_response[index]['ImageDescription'], project.ImageDescription)

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_2(self, user):
        """ На странице два проекта. Получаем вторую страницу."""
        limit, page, projects = 2, 1, list()

        image_id = await self.execute(
            insert(db.Image)
            .values(Title='Title', Link='Link', ImageLink='ImageLink', Description='ImageDescription', UserId=user.Id)
            .returning(db.Image.Id)
        )
        for project in range(10):
            project_obj = ProjectWithImage(
                    None, f'Name{project}', 'Description', datetime.now(), 'Repository', 'LongDescription',
                    user.Id, None, image_id, "Title", "Link", "ImageLink", "ImageDescription"
            )

            project_obj.Id = await self.execute(
                insert(db.Project)
                .values(
                    Name=project_obj.Name, Description=project_obj.Description,
                    LongDescription=project_obj.LongDescription, Date=project_obj.Date,
                    Repository=project_obj.Repository, Image=project_obj.Image, UserId=project_obj.UserId
                )
                .returning(db.Project.Id)
            )

            projects.append(project_obj)

        response = await self.invoke(
            GetProjectListWithImage,
            Page=page,
            Limit=limit
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
        date_response = await response.json()

        self.assertEqual(len(date_response), limit)
        self.assertEqual(date_response[0]['Name'], projects[len(projects)-3].Name)
        self.assertEqual(date_response[1]['Name'], projects[len(projects)-4].Name)
