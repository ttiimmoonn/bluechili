from datetime import datetime

from sqlalchemy import insert, select

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.auth.login import Login
from bluechili.bl.project.set import Set

from bluechili.core.model.user import User
from bluechili.core.account.permission import Permission

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestSet(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_1(self, user: User):
        """ Добавляем проект."""
        create_date = datetime.now()
        response = await self.invoke(
            Set,
            Id=None, Name='Name', Description='Description', Date=create_date, Repository='Repository',
            Image=None, LongDescription='LongDescription', Author=user.Id
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)

        result = await self.execute(select(db.Project))

        self.assertTrue(isinstance(result[0], int))
        self.assertEqual(result[1], 'Name')
        self.assertEqual(result[2], 'Description')
        self.assertEqual(result[3], 'LongDescription')
        self.assertEqual(result[4], create_date)
        self.assertEqual(result[5], 'Repository')
        self.assertEqual(result[6], None)
        self.assertEqual(result[7], user.Id)
        self.assertIsNotNone(isinstance(result[8], datetime))

    @unittest_run_loop
    @create_user(is_super=True)
    async def test_2(self, user: User):
        """ Обновляем проект."""
        create_date = datetime.now()

        project_id = await self.execute(
            insert(db.Project)
            .values(
                Name="Test", Description="Description", LongDescription="LongDescription", Date=datetime.now(),
                Repository="Repository", Image=None, UserId=user.Id
            )
            .returning(db.Project.Id)
        )

        response = await self.invoke(
            Set,
            Id=project_id, Name='Test1', Description='Description1', Date=create_date, Repository='Repository1',
            Image=None, LongDescription='LongDescription1', Author=user.Id
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
        self.assertEqual({'Id': project_id}, await response.json())

        result = await self.execute(select(db.Project))

        self.assertTrue(isinstance(result[0], int))
        self.assertEqual(result[1], 'Test1')
        self.assertEqual(result[2], 'Description1')
        self.assertEqual(result[3], 'LongDescription1')
        self.assertEqual(result[4], create_date)
        self.assertEqual(result[5], 'Repository1')
        self.assertEqual(result[6], None)
        self.assertEqual(result[7], user.Id)
        self.assertIsNotNone(isinstance(result[8], datetime))

    @unittest_run_loop
    @create_user(is_super=False, auth=False)
    async def test_3(self, user: User):
        """ Нет прав на работу с проектом."""
        # Вызов без авторизации
        response = await self.invoke(
            Set,
            Id=None, Name='Test1', Description='Description1', Date=datetime.now(), Repository='Repository1',
            Image=None, LongDescription='LongDescription1', Author=user.Id
        )
        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)

        # Вызов без прав
        await self.invoke(Login, Login=user.Login, Password=user.Password)

        response = await self.invoke(
            Set,
            Id=None, Name='Test1', Description='Description1', Date=datetime.now(), Repository='Repository1',
            Image=None, LongDescription='LongDescription1', Author=user.Id
        )
        self.assertEqual(response.status, web.HTTPForbidden.status_code)

        # Добавили права
        await self.execute(
            insert(db.Permission)
            .values(Name=Permission.PROJECT_EDIT, UserId=user.Id)
        )

        response = await self.invoke(
            Set,
            Id=None, Name='Test1', Description='Description1', Date=datetime.now(), Repository='Repository1',
            Image=None, LongDescription='LongDescription1', Author=user.Id
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
