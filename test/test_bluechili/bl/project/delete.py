from datetime import datetime

from sqlalchemy import insert, select

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.auth.login import Login
from bluechili.bl.project.delete import Delete

from bluechili.core.model.user import User
from bluechili.core.account.permission import Permission

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class DeleteSet(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user()
    async def test_1(self, user: User):
        """ Удалить проект."""
        project_id = await self.execute(
            insert(db.Project)
            .values(
                Name="Test", Description="Description", LongDescription="LongDescription", Date=datetime.now(),
                Repository="Repository", Image=None, UserId=user.Id
            )
            .returning(db.Project.Id)
        )

        response = await self.invoke(Delete, Id=project_id)

        self.assertEqual(response.status, web.HTTPOk.status_code)
        self.assertIsNone(await self.execute(select(db.Project)))

    @unittest_run_loop
    @create_user()
    async def test_2(self, user: User):
        """ Удаляем проект которого нет."""
        response = await self.invoke(Delete, Id=0)
        self.assertEqual(response.status, web.HTTPOk.status_code)

    @unittest_run_loop
    @create_user(is_super=False, auth=False)
    async def test_3(self, user: User):
        """ Проверка прав на удаление проекта."""
        project_id = await self.execute(
            insert(db.Project)
            .values(
                Name="Test", Description="Description", LongDescription="LongDescription", Date=datetime.now(),
                Repository="Repository", Image=None, UserId=user.Id
            )
            .returning(db.Project.Id)
        )

        # Вызов без авторизации
        response = await self.invoke(Delete, Id=project_id)

        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)

        # Вызов без прав
        await self.invoke(Login, Login=user.Login, Password=user.Password)

        response = await self.invoke(Delete, Id=project_id)
        self.assertEqual(response.status, web.HTTPForbidden.status_code)

        # Добавили права
        await self.execute(
            insert(db.Permission)
            .values(Name=Permission.PROJECT_DELETE, UserId=user.Id)
        )

        response = await self.invoke(Delete, Id=project_id)

        self.assertEqual(response.status, web.HTTPOk.status_code)
