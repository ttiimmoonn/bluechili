from datetime import datetime

from sqlalchemy import insert

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.project.get import Get
from bluechili.core.model.user import User

from test.test_helpers import db
from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestGet(CustomAioHTTPTestCase):

    @unittest_run_loop
    @create_user(is_super=False)
    async def test_1(self, user: User):
        """ Поучить проект."""
        project_id = await self.execute(
            insert(db.Project)
            .values(
                Name="Test", Description="Description", LongDescription="LongDescription", Date=datetime.now(),
                Repository="Repository", Image=None, UserId=user.Id
            )
            .returning(db.Project.Id)
        )

        response = await self.invoke(
            Get,
            Id=project_id
        )

        self.assertEqual(response.status, web.HTTPOk.status_code)
        date_response = await response.json()

        self.assertEqual(date_response['Id'], project_id)
        self.assertEqual(date_response['Name'], "Test")
        self.assertEqual(date_response['Description'], "Description")
        self.assertEqual(date_response['LongDescription'], "LongDescription")
        self.assertEqual(date_response['Repository'], "Repository")
        self.assertIsNone(date_response['Image'])
        self.assertEqual(date_response['UserId'], user.Id)
        self.assertIsNotNone(date_response['Create'], web.HTTPOk.status_code)
