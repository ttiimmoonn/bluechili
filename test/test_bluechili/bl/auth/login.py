from sqlalchemy import insert

from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.auth.login import Login

from test_helpers import db
from test_helpers.helpers import CustomAioHTTPTestCase


class TestLogin(CustomAioHTTPTestCase):

    @unittest_run_loop
    async def test_1(self):
        """ Клиента не существует. Получаем ошибку авторизации."""
        response = await self.invoke(Login, Login='test', Password='test')
        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)

    @unittest_run_loop
    async def test_2(self):
        """ Клиент существует. Неверный пароль."""
        await self.execute(insert(db.User).values(Login='test', Password=self._PASSWORD_HASH['password']))
        response = await self.invoke(Login, Login='test', Password='test')
        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)

    @unittest_run_loop
    async def test_3(self):
        """ Клиент существует. Корректный пароль. Авторизация проходит успешно."""
        await self.execute(insert(db.User).values(Login='test', Password=self._PASSWORD_HASH['password']))
        response = await self.invoke(Login, Login='test', Password='password')
        self.assertEqual(response.status, web.HTTPOk.status_code)
        self.assertIsNotNone(response.headers['Set-Cookie'])
