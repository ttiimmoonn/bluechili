from aiohttp import web
from aiohttp.test_utils import unittest_run_loop

from bluechili.bl.auth.logout import Logout

from test.test_helpers.helpers import CustomAioHTTPTestCase, create_user


class TestLogout(CustomAioHTTPTestCase):

    @unittest_run_loop
    async def test_1(self):
        """ Выходим из под пользователя, но пользователя нет. Ошибка выхода."""
        response = await self.invoke(Logout)
        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)

    @unittest_run_loop
    @create_user()
    async def test_2(self, user):
        """ Есть авторизованный клиент. Выходим из под него."""
        # Выходим из под пользователя
        response = await self.invoke(Logout)
        self.assertEqual(response.status, web.HTTPOk.status_code)

        # Авторизации уже нет, ошибка при выходе из под пользователя
        response = await self.invoke(Logout)
        self.assertEqual(response.status, web.HTTPUnauthorized.status_code)
