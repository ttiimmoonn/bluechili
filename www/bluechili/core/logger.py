""" Модуль логгирования. """

import os
import logging

from pathlib import PurePath

__author__ = 'Ушаков Т.Л.'

__all__ = ['create_logger']

# Стандартный формат логгера
DEFAULT_FORMATTER = logging.Formatter("%(asctime)-8s %(levelname)-6s [%(name)-2s|%(module)s:%(lineno)d] %(message)-8s")


def create_logger(path: PurePath, name: str = 'chat_bot', level: int = logging.INFO) -> logging.Logger:
    """ Создать логгер.

    :param path: Путь до директории с логиами
    :param name: Имя логгера
    :param level: Уровень логирования
    """
    if not os.path.exists(os.fspath(path)):
        os.makedirs(os.fspath(path))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(_get_logger_handler_stream(level))
    logger.addHandler(_get_logger_handler_file(path, level))

    return logger


def _get_logger_handler_file(path: PurePath, level: int, formatter: str = DEFAULT_FORMATTER) -> logging.FileHandler:
    """ Получить файловый логгер обработчик ошибок.

    :param path: Путь до директории с логиами
    :param level: Уровень логирования
    :param formatter: Формат логов
    """
    file_path = path / f'log_{level}.log'
    return _get_logger_handler(logging.FileHandler(os.fspath(file_path), 'w'), level, formatter)


def _get_logger_handler_stream(level: int, formatter: str = DEFAULT_FORMATTER) -> logging.StreamHandler:
    """ Получить консольный логгер обработчик ошибок.

    :param level: Уровень логирования
    :param formatter: Формат логов
    """
    return _get_logger_handler(logging.StreamHandler(), level, formatter)


def _get_logger_handler(
        handler: [logging.StreamHandler, logging.FileHandler],
        level: int,
        formatter: str
) -> [logging.StreamHandler, logging.FileHandler]:
    """ Получить log обработчик.

    :param handler: Обработчик логов
    :param level: Уровень логирования
    :param formatter: Формат логов
    """
    handler.setLevel(level)
    handler.setFormatter(formatter)
    return handler
