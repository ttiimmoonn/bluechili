""" Права доступа."""

__all__ = ['Permission']


class Permission:
    """ Все права доступа в приложении."""

    # Создание и изменение пользователей
    USER_EDIT = 'user-edit'
    # Удаление пользователей
    USER_DELETE = 'user-delete'

    # Создание и изменение проектов
    PROJECT_EDIT = 'project-edit'
    # Удаление проектов
    PROJECT_DELETE = 'project-delete'

    # Установка прав у пользователя
    PERMISSION_EDIT = 'permission-edit'
    # Удаление прав у пользователя
    PERMISSION_DELETE = 'permission-delete'
