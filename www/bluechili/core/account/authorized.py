""" Модуль авторизации."""

from typing import Optional

from passlib.hash import sha256_crypt
from aiohttp_security.abc import AbstractAuthorizationPolicy

from bluechili.core.model.user import User

__author__ = "Ушаков Т.Л."

__all__ = ['DBAuthorizationPolicy', 'check_credentials']


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, db):
        self.db = db

    async def authorized_userid(self, identity: str) -> Optional[str]:
        """ Вернуть идентификатор авторизованного пользователя.
        Вернуть user_id авторизованного пользователя или None если не существует
        пользователя связанного с этим идентификатором.

        :param identity: Идентификатор пользователя.
        """
        if not isinstance(identity, str):
            return None

        async with self.db.cursor() as cursor:
            await cursor.execute(_SQL_CHECK_USER, {'login': identity})
            check_user = await cursor.fetchone()

            if bool(check_user[0]):
                return identity
            return None

    async def permits(self, identity: str, permission: str, context: Optional[str] = None) -> bool:
        """ Проерить права пользователя.
        Вернут True если пользователь имеет права в текущем контексте, иначе False.

        :param identity: Идентификатор пользователя;
        :param permission: Название права;
        :param context: Контекст.
        """
        if identity is None or not isinstance(identity, str):
            return False

        async with self.db.cursor() as cursor:
            await cursor.execute(_SQL_GET_USER, {'login': identity})
            user_data = await cursor.fetchone()
            if user_data is None:
                return False

            user = User(*user_data)
            if user.IsSuperuser:
                return True

            await cursor.execute(
                _SQL_CHECK_PERMISSION,
                {'user_id': user.Id, 'permission': permission}
            )

            check_permission = await cursor.fetchone()
            if bool(check_permission[0]):
                return True
            return False


async def check_credentials(db, identity: str, password: str) -> bool:
    """ Проверить данные авторизации пользователя
    :param db: Указатель на БД;
    :param identity: Логин пользователя;
    :param password: Пароль.
    """
    async with db.cursor() as cursor:
        await cursor.execute(_SQL_GET_USER, {'login': identity})
        user_data = await cursor.fetchone()

    if user_data is None:
        return False
    user = User(*user_data)

    try:
        return sha256_crypt.verify(password, user.Password)
    except ValueError:
        return False

# Проверить работоспособность пользователя в базе.
# login - логин пользователя
_SQL_CHECK_USER = """
    SELECT
        count(*) as "isAccess"
    FROM 
        "User"
    WHERE 
        "Login" = %(login)s
        AND "Disabled" is False;
"""

# Получить пользователя по идентификатору
# login - логин пользователя
_SQL_GET_USER = """
    SELECT 
        "@User", "Login", "Password", "IsSuperuser"
    FROM 
        "User"
    WHERE 
        "Login" = %(login)s
        AND "Disabled" is False;
"""

# Проверить права пользователя
# user_id - идентификатор пользователя
# permission - название права для проверки
_SQL_CHECK_PERMISSION = """
    SELECT
        count(*) as "isAccess"
    FROM
        "Permission"
    WHERE
        "UserId" = %(user_id)s
        AND "Name" = %(permission)s;
"""
