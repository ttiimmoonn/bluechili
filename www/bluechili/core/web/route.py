""" Получение маршрутов для приожения."""

from aiohttp import web
from aiohttp_cors import ResourceOptions, setup

from bluechili.bl.auth.status import Status
from bluechili.bl.auth.login import Login
from bluechili.bl.auth.logout import Logout

from bluechili.bl.user.get_by_user_id import GetByUserId as UserGetByUserId

from bluechili.bl.permission.delete_by_user_id import DeleteByUserId as PermissionDeleteByUserId
from bluechili.bl.permission.get_by_user_id import GetByUserId as PermissionGetByUserId
from bluechili.bl.permission.set_by_user_id import SetByUserId as PermissionSetByUserId

from bluechili.bl.project.get import Get as ProjectGet
from bluechili.bl.project.set import Set as ProjectSet
from bluechili.bl.project.delete import Delete as ProjectDelete

from bluechili.bl.registry.get_project_list_with_image import GetProjectListWithImage

__author__ = 'Ушаков Т.Л.'

__all__ = ['setup_routes']

# Список подключенных методов бизнес логики.
BUSINESS_LOGIC = [
    Status,
    Login,
    Logout,
    UserGetByUserId,
    PermissionDeleteByUserId,
    PermissionGetByUserId,
    PermissionSetByUserId,
    ProjectGet,
    ProjectSet,
    ProjectDelete,
    GetProjectListWithImage
]

# Политика кросс-доменных запросов.
SAME_ORIGIN_POLICY = {
    "*": ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*"
    )
}


def setup_routes(application: web.Application) -> None:
    """ Добавляем бизнес-логики в приложение.

    :param application: Web приложение.
    """
    cors = setup(application, defaults=SAME_ORIGIN_POLICY)
    for bl in BUSINESS_LOGIC:
        cors.add(application.router.add_route(*bl.get_route()))
