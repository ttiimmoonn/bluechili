""" Описание бизнес-логики."""

import json
from aiohttp import web
from psycopg2 import ProgrammingError
from aiohttp_security import check_authorized, check_permission

from bluechili.core.account.permission import Permission

__author__ = 'Ушаков Т.Л.'

__all__ = ['BusinessLogic', 'MethodBL']


class MethodBL:
    """ Http методы доступные в бизнес логике."""
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATH = "PATH"


class BusinessLogic(web.View):
    """ Метакласс для описания бизнес логики."""

    # Маршрут.
    ROUTE = str()
    # Метод
    METHOD = MethodBL.GET

    async def get(self) -> web.StreamResponse:
        """ Базовая реализация метода бизнес логики.
        Если действия не заданы, то возвращаем 501 код - 'Метод не поддерживается'.
        """
        return web.StreamResponse(status=501, reason='HTTPNotImplemented')

    async def post(self) -> web.StreamResponse:
        """ Базовая реализация метода бизнес логики.
        Если действия не заданы, то возвращаем 501 код - 'Метод не поддерживается'.
        """
        return web.StreamResponse(status=501, reason='HTTPNotImplemented')

    @classmethod
    def get_route(cls) -> tuple:
        """ Получить из бизнес-логики маршрут для приложения.
        :return: Список в корректном формате для построения маршрута.
        """
        return cls.METHOD, cls.ROUTE, cls

    async def sql_query(self, sql: str, params: [dict, None] = None) -> [None, dict, list]:
        """ Выполнить sql запрос.

        :param sql: Запрос;
        :param params: Именнованные параметры запроса;
        """
        async with self.request.app.db.cursor() as cursor:
            try:
                if params is None:
                    await cursor.execute(sql)
                else:
                    await cursor.execute(sql, params)

                try:
                    result = await cursor.fetchall()
                except ProgrammingError:
                    return None

            except ProgrammingError:
                raise web.HTTPBadRequest()
        return result

    async def sql_query_scalar(self, sql: str, params: [dict, None] = None) -> [None, dict, list]:
        """ Выполнить sql запрос. Вернуть только одно значение.

        :param sql: Запрос;
        :param params: Именнованные параметры запроса;
        """
        async with self.request.app.db.cursor() as cursor:
            try:
                if params is None:
                    await cursor.execute(sql)
                else:
                    await cursor.execute(sql, params)

                try:
                    result = await cursor.fetchone()
                    if isinstance(result, tuple) and len(result) == 1:
                        result = result[0]
                except ProgrammingError:
                    return None

            except ProgrammingError:
                raise web.HTTPBadRequest()

        if result is None:
            return None
        return result

    @staticmethod
    def check_authorized(get_user_login: bool = False):
        """ Проверить авторизацию в вызове.

        :param get_user_login: Передавать ли в метод Bl логин пользователя.
        """
        def decorator(func):
            async def wrapper(self):
                login = await check_authorized(self.request)
                if get_user_login:
                    return await func(self, login)
                return await func(self)
            return wrapper
        return decorator

    @staticmethod
    def check_permission(permissions: list):
        """ Проверить разрешения у метода БЛ.

        :param permissions: Список разрешений для проверки.
        """
        def decorator(func):
            async def wrapper(self):
                if not isinstance(permissions, list):
                    raise web.HTTPBadRequest()

                for permission in permissions:
                    if permission not in list(Permission.__dict__.values()):
                        raise web.HTTPBadRequest()
                    await check_permission(self.request, permission)

                return await func(self)
            return wrapper
        return decorator

    @staticmethod
    def parser_json_params(*required_param: tuple):
        """ Получить параметры у для метода.

        :param required_param: Парамтры вызова функции.
        """
        def decorator(func):
            async def wrapper(self):
                result = list()
                try:
                    permission_data = await self.request.json()
                except json.decoder.JSONDecodeError:
                    raise web.HTTPBadRequest()

                for req_param in required_param:
                    if req_param not in permission_data:
                        raise web.HTTPBadRequest()
                    result.append(permission_data.get(req_param))

                return await func(self, *result)
            return wrapper
        return decorator
