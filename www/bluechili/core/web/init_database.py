""" Взаимодействие с базой данных."""

import aiopg

from bluechili.core.setting import Settings

__author__ = "Ушаков Т.Л."

__all__ = ['init_database']


def get_dsn(name, user, password, host):
    return f"dbname={name} user={user} password={password} host={host}"


async def init_database(setting: Settings):
    """ Инициализировать базу данных согласно схеме и получить указатель.

    :param setting: Название базы данных;
    :return: Указатель на базу данных.
    """
    pool = await aiopg.create_pool(
        get_dsn(setting.db_name, setting.db_user, setting.db_password, setting.db_host)
    )
    connect = await pool.acquire()

    return connect
