""" Модуль для формирования настроек для сервера."""

from aiohttp import web
from aiohttp_session import setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage

from aiohttp_security import setup as setup_security
from aiohttp_security import SessionIdentityPolicy

from bluechili.core.web.init_database import init_database
from bluechili.core.setting import Settings
from bluechili.core.web.route import setup_routes
from bluechili.core.web.background_task import setup_background_task
from bluechili.core.account.authorized import DBAuthorizationPolicy

__author__ = 'Ушаков Т.Л.'

__all__ = ['init_application']


async def init_application(settings: Settings) -> web.Application:
    """ Инициализация web приложения.

    :param settings: Настройка сервера из .env
    :return: Приложение готовое для запуска
    """
    application = web.Application()

    application.setting = settings
    application.db = await init_database(settings)

    setup(application,
          EncryptedCookieStorage(settings.encrypted_cookie_storage))
    setup_security(application,
                   SessionIdentityPolicy(),
                   DBAuthorizationPolicy(application.db))
    setup_routes(application)
    setup_background_task(application)

    return application
