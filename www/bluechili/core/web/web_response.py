""" Переопределение стандартных ответов aiohttp."""

import json

from datetime import date, datetime

from typing import (
    Any,
    Optional
)

from aiohttp.helpers import sentinel
from aiohttp.web_response import Response
from aiohttp.typedefs import JSONEncoder, LooseHeaders

__author__ = 'Ушаков Т.Л.'

__all__ = ['json_response', 'default_serializable_json']


def default_serializable_json(param):
    if isinstance(param, (date, datetime)):
        return param.isoformat()


def json_response(
    data: Any = sentinel,
    *,
    text: Optional[str] = None,
    body: Optional[bytes] = None,
    status: int = 200,
    reason: Optional[str] = None,
    headers: Optional[LooseHeaders] = None,
    content_type: str = "application/json",
    dumps: JSONEncoder = json.dumps
) -> Response:
    """
    Вернуть данные в формате Json.

    :param data: Тело ответа для упаковки в json
    :param text: Тело ответа в формате str
    :param body: Тело ответа в формате bytes
    :param status: HTTP код статус
    :param reason: HTTP текстовое описание
    :param headers: Заголовки
    :param content_type: Тип контента
    :param dumps: Функция серелизации в Json строку
    """
    if data is not sentinel:
        if text or body:
            raise ValueError("only one of data, text, or body should be specified")
        else:
            text = dumps(data, default=default_serializable_json)
    return Response(
        text=text,
        body=body,
        status=status,
        reason=reason,
        headers=headers,
        content_type=content_type,
    )
