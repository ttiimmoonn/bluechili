""" Добавить фоновые задачи в приложение."""

from asyncio import create_task
from aiohttp import web

from bluechili.core import background_tasks

# Фоновые задачи для добавления в приложение
BG_TASKS = []


def setup_background_task(application: web.Application) -> None:
    """ Добавить в приложение фоновые задачи.

    :param application: Web приложение.
    """
    application.on_startup.append(start_background_tasks)
    application.on_cleanup.append(cleanup_background_tasks)


async def start_background_tasks(application: web.Application) -> None:
    """ Добавляем фоновые задачи которые запустятся при запуске приложения.

    :param application: Web приложение.
    """
    for task in BG_TASKS:
        application[task.__name__] = create_task(task(application))


async def cleanup_background_tasks(application: web.Application) -> None:
    """ Добавляем фоновые задачи которые запустятся при закрытии приложения.

    :param application: Web приложение.
    """
    for task in BG_TASKS:
        application[task.__name__].cancel()
        await application[task.__name__]
