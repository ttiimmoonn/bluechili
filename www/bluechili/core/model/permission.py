""" Модель разрешений."""

from datetime import datetime
from dataclasses import dataclass

__author__ = "Ушаков Т.Л."
__all__ = ['Permission']


@dataclass
class Permission:
    """ Модель разрешения."""

    # Идентификатор разрешения
    Id: int
    # Идентификатор пользователя
    UserId: int
    # Имя разрешения
    Name: str
    # Дата добавления проекта в базу
    Create: datetime
