""" Модель пользователя."""

from dataclasses import dataclass

__author__ = "Ушаков Т.Л."
__all__ = ['User']


@dataclass
class User:
    """ Модель пользователя."""

    # Идентификатор пользователя
    Id: int
    # Логин
    Login: str
    # Пароль
    Password: str
    # Является ли пользователь супер-пользователем
    IsSuperuser: bool
