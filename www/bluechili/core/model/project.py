""" Модель проектов."""

from datetime import datetime
from dataclasses import dataclass

__author__ = "Ушаков Т.Л."
__all__ = ['Project', 'ProjectWithImage']


@dataclass
class Project:
    """ Модель проекта."""

    # Идентификатор проекта
    Id: int
    # Имя проекта
    Name: str
    # Краткое описание проекта
    Description: str
    # Дата работы над проеком
    Date: datetime
    # Ссылка на репозиторий проекта
    Repository: str
    # Полное описание проекта
    LongDescription: str
    # Идентификатор пользователя создавшего проекта
    UserId: int
    # Дата добавления проекта в базу
    Create: datetime
    # Идентификатор изображения проекта
    Image: int


@dataclass
class ProjectWithImage:
    """ Модель проекта с изображением."""

    # Идентификатор проекта
    Id: int
    # Имя проекта
    Name: str
    # Краткое описание проекта
    Description: str
    # Дата работы над проеком
    Date: datetime
    # Ссылка на репозиторий проекта
    Repository: str
    # Полное описание проекта
    LongDescription: str
    # Идентификатор пользователя создавшего проекта
    UserId: int
    # Дата добавления проекта в базу
    Create: datetime
    # Идентификатор изображения проекта
    Image: int
    # Название изображения
    Title: str
    # Ссылка на директорию с изображением
    Link: str
    # Прямая ссылка на изображение
    ImageLink: str
    # Описание изображения
    ImageDescription: str
