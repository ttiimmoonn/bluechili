""" Настройка сервера."""

from pydantic import BaseSettings, Field

__author__ = "Ушаков Т.Л."

__all__ = ['Settings']


class Settings(BaseSettings):
    """ Насйтрока сервера."""

    # IP адрес сервера
    server_host: str = Field(..., env='SERV_HOST')
    # Порт сервера
    server_port: int = Field(..., env='SERV_PORT')

    # Адрес БД
    db_host: str = Field('localhost', env='PG_DB_ADDRESS')
    # Порт БД
    db_port: int = Field(5432, env='PG_DB_PORT')
    # Имя БД
    db_name: str = Field('bluechili', env='PG_DB_NAME')
    # Имя пользователя для доступа к БД
    db_user: str = Field('postgres', env='PG_DB_USER')
    # Пароль для доступа пользователя к БД
    db_password: str = Field('postgres', env='PG_DB_PASSWORD')
    # Имя схемы БД
    db_schema: str = Field('public', env='PG_DB_SCHEMA')
    # Ключ шифрования куки файлов
    encrypted_cookie_storage: bytes = Field(..., env='SERV_ENCRYPTED_COOKIE_STORAGE')

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
