""" Метод бизнес логики."""

import logging

from aiohttp import web
from aiohttp_security import remember

from bluechili.core.account.authorized import check_credentials
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['Login']

logger = logging.getLogger("bl_logger")


class Login(BusinessLogic):
    """ Авторизоваться."""

    ROUTE = '/api/v1/auth/login'
    METHOD = MethodBL.POST

    @BusinessLogic.parser_json_params('Login', 'Password')
    async def post(self, login: str, password: str) -> web.HTTPException:
        """ Реализация бизнес логики.

        :param login: Идентификатор пользователя.
        :param password: Пароль пользователя.
        """
        response = web.HTTPOk()

        if await check_credentials(self.request.app.db, login, password):
            await remember(self.request, response, login)
            return response

        return web.HTTPUnauthorized()
