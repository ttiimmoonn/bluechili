""" Метод бизнес логики."""

import logging

from aiohttp.web_response import Response

from bluechili.core.web import json_response
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['Status']

logger = logging.getLogger("bl_logger")


class Status(BusinessLogic):
    """ Получить текущего пользователя по сессии."""

    ROUTE = '/api/v1/auth/status'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized(True)
    async def post(self, login: str) -> Response:
        """ Реализация бизнес логики.

        :param login: Логин пользователя полученный по сессии.
        :return: Логин пользователя
        """
        return json_response({"Login": login})
