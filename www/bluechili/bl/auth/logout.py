""" Метод бизнес логики."""

import logging

from aiohttp import web
from aiohttp_security import forget

from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['Logout']

logger = logging.getLogger("bl_logger")


class Logout(BusinessLogic):
    """ Разлогиниться."""

    ROUTE = '/api/v1/auth/logout'
    METHOD = MethodBL.GET

    @BusinessLogic.check_authorized()
    async def get(self) -> web.HTTPException:
        """ Реализация бизнес логики."""
        response = web.HTTPOk()
        await forget(self.request, response)
        return response
