""" Метод бизнес логики."""

import logging

from typing import Optional
from dataclasses import asdict

from bluechili.core.model.project import ProjectWithImage

from bluechili.core.web import json_response
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['GetProjectListWithImage']

logger = logging.getLogger("bl_logger")

# Количество проектов на странице
DEFAULT_PROJECT_LIMIT = 100

# Номер страницы
DEFAULT_PAGE = 0


class GetProjectListWithImage(BusinessLogic):
    """ Получить список проектов."""

    ROUTE = '/api/v1/registry/get_project_list_with_image'
    METHOD = MethodBL.POST

    @BusinessLogic.parser_json_params('Page', 'Limit')
    async def post(self, page: Optional[int], project_limit: Optional[int]):
        """ Реализация бизнес логики.

        :param page:
        :param project_limit:
        """
        if page is None:
            page = DEFAULT_PAGE

        if project_limit is None:
            project_limit = DEFAULT_PROJECT_LIMIT

        projects = await self.sql_query(
            _SQL_GET_PROJECT_LIST_WITH_IMAGE,
            {
                'limit': project_limit,
                'offset': page * project_limit
            }
        )

        result = list()

        if projects is None:
            return json_response(result)

        for project in projects:
            result.append(asdict(ProjectWithImage(*project)))

        return json_response(result)


# Получить список проектов с изображениями.
# limit - количество проектов на одной странице.
# offset - количество проекто, которые нужно пропустить.
_SQL_GET_PROJECT_LIST_WITH_IMAGE = """
    SELECT
        "Project".*, "Image"."Title", "Image"."Link", "Image"."ImageLink", "Image"."Description" AS "ImageDescription"
    FROM
        "Project"
    LEFT JOIN "Image" ON "Project"."Image" = "Image"."@Image" 
    ORDER BY "@Project" DESC
    LIMIT %(limit)s::INT
    OFFSET %(offset)s::INT
"""
