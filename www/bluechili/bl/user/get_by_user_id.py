""" Метод бизнес логики."""

import logging

from dataclasses import asdict
from aiohttp.web_response import Response

from bluechili.core.model.user import User

from bluechili.core.web import json_response
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['GetByUserId']

logger = logging.getLogger("bl_logger")


class GetByUserId(BusinessLogic):
    """ Получить пользователя по идентификатору пользователя."""

    ROUTE = '/api/v1/user/get_by_user_id'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.parser_json_params('UserId')
    async def post(self, user_id: int) -> Response:
        user = await self.sql_query_scalar(_SQL_GET_USER_BY_ID, {'UserId': user_id})

        if user is None:
            return json_response(dict())

        return json_response(asdict(User(*user)))


# Получить пользователя по идентификатору
# UserId - id в таблице пользователей
_SQL_GET_USER_BY_ID = """
    SELECT 
        "@User", "Login", "IsSuperuser", "Disabled"
    FROM 
        "User"
    WHERE 
        "@User" = %(UserId)s::INT
"""
