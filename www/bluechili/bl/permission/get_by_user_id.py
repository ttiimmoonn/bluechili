""" Метод бизнес логики."""

import logging

from dataclasses import asdict

from aiohttp.web_response import Response

from bluechili.core.web import json_response
from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

from bluechili.core.model.permission import Permission as PermissionModel

__all__ = ['GetByUserId']

logger = logging.getLogger("bl_logger")


class GetByUserId(BusinessLogic):
    """ Получить все разрешения пользователя по идентификатору пользователя."""

    ROUTE = '/api/v1/permission/get_by_user_id'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.check_permission([Permission.PERMISSION_EDIT])
    @BusinessLogic.parser_json_params('UserId')
    async def post(self, user_id) -> Response:
        """ Реализация бизнес логики.

        :param user_id: Идентификатор пользователя.
        """
        permissions = await self.sql_query(_SQL_GET_PERMISSION_BY_USER_ID, {'UserId': user_id})
        if permissions is None:
            return json_response(list())

        return json_response([asdict(PermissionModel(*permission)) for permission in permissions])


# Получить все разрешения по идентификатору пользователя
# UserId - Идентификатор пользователя.
_SQL_GET_PERMISSION_BY_USER_ID = """
    SELECT 
        *
    FROM
        "Permission"
    WHERE
        "UserId" = %(UserId)s::INT;
"""
