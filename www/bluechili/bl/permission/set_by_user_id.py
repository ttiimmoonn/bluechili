""" Метод бизнес логики."""

import logging

from aiohttp import web

from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['SetByUserId']

logger = logging.getLogger("bl_logger")


class SetByUserId(BusinessLogic):
    """ Установить разрешение пользователю по идентификатору пользователя."""

    ROUTE = '/api/v1/permission/set_by_user_id'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.check_permission([Permission.PERMISSION_EDIT])
    @BusinessLogic.parser_json_params('UserId', 'PermissionName')
    async def post(self, user_id: int, permission_name: str) -> web.HTTPException:
        """ Реализация бизнес логики.

        :param user_id: Идентификатор пользователя.
        :param permission_name: Название разрешения.
        """
        if permission_name not in list(Permission.__dict__.values()):
            return web.HTTPBadRequest()

        await self.sql_query(
            _SET_PERMISSION_BY_USER_ID,
            {'UserId': user_id, 'PermissionName': permission_name}
        )
        return web.HTTPOk()


# Установить новую роль пользователю по идентификатору пользователя.
# UserId - Идентификатор пользователя.
# PermissionName - Название разрешения.
_SET_PERMISSION_BY_USER_ID = """
    INSERT INTO "Permission" ("UserId", "Name")
    VALUES (%(UserId)s::INT, %(PermissionName)s::TEXT)
"""
