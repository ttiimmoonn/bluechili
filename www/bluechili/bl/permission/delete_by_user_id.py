""" Метод бизнес логики."""

import logging

from aiohttp import web

from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['DeleteByUserId']

logger = logging.getLogger("bl_logger")


class DeleteByUserId(BusinessLogic):
    """ Удалить разрешение у пользователя по идентификатору пользователя."""

    ROUTE = '/api/v1/permission/delete_by_user_id'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.check_permission([Permission.PERMISSION_DELETE])
    @BusinessLogic.parser_json_params('UserId', 'PermissionName')
    async def post(self, user_id: int, permission_name: str) -> web.HTTPException:
        """ Реализация бизнес логики.

        :param user_id: Идентификатор пользователя.
        :param permission_name:  Название разрешения.
        """
        await self.sql_query(
            _SQL_DELETE_PERMISSION_BY_USER_ID,
            {'UserId': user_id, 'PermissionName': permission_name}
        )
        return web.HTTPOk()


# Удалить разрешение у пользователя
# UserId - Идентификатор пользователя.
# PermissionName - Имя разрешения.
_SQL_DELETE_PERMISSION_BY_USER_ID = """
    DELETE FROM "Permission"
    WHERE
        "UserId" = %(UserId)s::INT
        AND "Name" = %(PermissionName)s::TEXT;
"""
