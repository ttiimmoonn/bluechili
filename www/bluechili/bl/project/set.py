""" Метод бизнес логики."""

import logging

from typing import Optional
from aiohttp.web_response import Response

from bluechili.core.web import json_response
from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['Set']

logger = logging.getLogger("bl_logger")


class Set(BusinessLogic):
    """ Изменить или создать проект."""

    ROUTE = '/api/v1/project/set'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.check_permission([Permission.PROJECT_EDIT])
    @BusinessLogic.parser_json_params(
        'Id', 'Name', 'Description',
        'Date', 'Repository', 'Image',
        'LongDescription', 'Author'
    )
    async def post(
        self,
        project_id: Optional[int],
        name: str,
        description: str,
        date: str,
        repository: str,
        image: int,
        long_description: str,
        author: int
    ) -> Response:
        """ Реализация бизнес логики.

        :param project_id: Идентификатор проекта
        :param name: Имя проекта
        :param description: Краткое описание проекта
        :param date: Дата работы над проеком
        :param repository: Ссылка на репозиторий проекта
        :param image: Полное описание проекта
        :param long_description: Полное описание проекта
        :param author: Идентификатор пользователя создавшего проекта
        :return: Идентификатор измененного или добавленного проекта
        """
        project_id = await self.sql_query_scalar(
            _SQL_UPSERT_PROJECT,
            {
                'Id': project_id, 'Name': name, 'Description': description, 'Date': date, 'Repository': repository,
                'Image': image, 'LongDescription': long_description, 'Author': author
            }
        )
        return json_response({'Id': project_id})


# Добавить или обновить проект
# Id - Идентификатор проекта
# Name - Имя проекта
# Description - Краткое описание проекта
# Date - Дата работы над проеком
# Repository - Ссылка на репозиторий проекта
# Image - Идентификатор изображения проекта
# LongDescription - Полное описание проекта
# Author - Идентификатор пользователя создавшего проекта
_SQL_UPSERT_PROJECT = """
WITH ExistedProject AS 
(
    SELECT
        * 
    FROM
        "Project" 
    WHERE
        "@Project" = %(Id)s::INT
    LIMIT 1 
), UpdateProject AS (
    UPDATE
        "Project"   
    SET
        "Name" = %(Name)s::TEXT, 
        "Description" = %(Description)s::TEXT, 
        "Date" = %(Date)s::TIMESTAMP, 
        "Repository" = %(Repository)s::TEXT, 
        "Image" = %(Image)s::INT, 
        "LongDescription" = %(LongDescription)s::TEXT,
        "UserId" = %(Author)s::INT
    WHERE
        EXISTS ( TABLE ExistedProject )
        AND "@Project" = %(Id)s::INT
    RETURNING "@Project" AS "Update" 
), InsertProject AS (
    INSERT INTO
        "Project" ( 
            "Name", 
            "Description", 
            "Date",
            "Repository", 
            "Image", 
            "LongDescription", 
            "UserId" 
        ) ( 
            SELECT
                %(Name)s::TEXT AS "Name", 
                %(Description)s::TEXT AS "Description", 
                %(Date)s::TIMESTAMP AS "Date", 
                %(Repository)s::TEXT AS "Repository", 
                %(Image)s::INT AS "Image", 
                %(LongDescription)s AS "LongDescription", 
                %(Author)s AS "UserId" 
            WHERE
                NOT EXISTS ( TABLE ExistedProject )
                AND NOT EXISTS ( TABLE UpdateProject )
        ) 
    RETURNING "@Project" AS "Insert" 
)
SELECT
    "Update" AS "Saved" 
FROM
    UpdateProject 
UNION
SELECT
    "Insert" AS "Saved" 
FROM
    InsertProject
"""
