""" Метод бизнес логики."""

import logging

from aiohttp import web

from bluechili.core.account.permission import Permission
from bluechili.core.web.business_logic import BusinessLogic, MethodBL


__all__ = ['Delete']

logger = logging.getLogger("bl_logger")


class Delete(BusinessLogic):
    """ Удалить проект."""

    ROUTE = '/api/v1/project/delete'
    METHOD = MethodBL.POST

    @BusinessLogic.check_authorized()
    @BusinessLogic.check_permission([Permission.PROJECT_DELETE])
    @BusinessLogic.parser_json_params('Id')
    async def post(self, project_id: int) -> web.HTTPException:
        """ Реализация бизнес логики.

        :param project_id: Идентификатор проекта
        """
        await self.sql_query_scalar(_SQL_DELETE_BY_PROJECT_ID, {'Id': project_id})
        return web.HTTPOk()


# Получить проект по идентификатору.
# Id - Идентификато проекта.
_SQL_DELETE_BY_PROJECT_ID = """
    DELETE FROM "Project"
    WHERE
        "@Project" = %(Id)s::INT;
"""
