""" Метод бизнес логики."""

import logging

from dataclasses import asdict

from aiohttp.web_response import Response

from bluechili.core.model.project import Project

from bluechili.core.web import json_response
from bluechili.core.web.business_logic import BusinessLogic, MethodBL

__all__ = ['Get']

logger = logging.getLogger("bl_logger")


class Get(BusinessLogic):
    """ Получить информацию по проекту."""

    ROUTE = '/api/v1/project/get'
    METHOD = MethodBL.POST

    @BusinessLogic.parser_json_params('Id')
    async def post(self, project_id: int) -> Response:
        """ Реализация бизнес логики.

        :param project_id: Идентификатор проекта
        :return: Вернет json с проектом по модели Project, если проект найден и пустой json если нет.
        """
        project = await self.sql_query_scalar(_SQL_GET_BY_PROJECT_ID, {'Id': project_id})

        if project is None:
            return json_response(dict())

        return json_response(asdict(Project(*project)))


# Получить проект по идентификатору.
# Id - Идентификатор проекта.
_SQL_GET_BY_PROJECT_ID = """
    SELECT
        *
    FROM
        "Project"
    WHERE
        "@Project" = %(Id)s::INT;
"""
