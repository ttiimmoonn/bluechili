""" Точка входа для запуска приложения."""

import asyncio
import logging

from pathlib import PurePath

from aiohttp import web

from bluechili.core.setting import Settings
from bluechili.core.logger import create_logger
from bluechili.core.web.init_application import init_application

__author__ = "Ушаков Т.Л."

__all__ = ['main']

logger = create_logger(PurePath('.', '/log'), 'bl_logger', logging.DEBUG)


def main() -> None:
    """ Точка входа для запуска web приложения.
    Блокирующая функция.
    """
    logger.info('Сервер запускается...')
    loop = asyncio.get_event_loop()
    application = loop.run_until_complete(init_application(Settings()))

    web.run_app(
        application,
        host=application.setting.server_host,
        port=application.setting.server_port
    )


if __name__ == '__main__':
    main()
