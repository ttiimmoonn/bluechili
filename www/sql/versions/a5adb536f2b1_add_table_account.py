"""Добавлена таблица Account.

Revision ID: a5adb536f2b1
Revises: 1313db4781b0
Create Date: 2021-03-16 01:22:39.411357

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a5adb536f2b1'
down_revision = '1313db4781b0'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'Account',
        sa.Column('@Account', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('FirstName', sa.String(300), nullable=False),
        sa.Column('LastName', sa.String(300), nullable=False),
        sa.Column('UserId', sa.Integer),
        sa.Column('Status', sa.String(300), nullable=False),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now()),
        sa.Column('Description', sa.TEXT, nullable=False),
        sa.Column('LongDescription', sa.TEXT, nullable=False)
    )

    # Ссылка на таблицу пользователей
    op.create_foreign_key('fk_user_id', 'Account', 'User', ['UserId'], ['@User'], ondelete='CASCADE')


def downgrade():
    op.drop_table('Account')
