"""Добавлена таблица ProjectRule.

Revision ID: c15b03627908
Revises: a5adb536f2b1
Create Date: 2021-03-16 01:28:35.816788

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c15b03627908'
down_revision = 'a5adb536f2b1'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'ProjectRule',
        sa.Column('@ProjectRule', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('ProjectId', sa.Integer),
        sa.Column('AccountId', sa.Integer),
        sa.Column('Rule', sa.String(300)),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now())
    )

    # Ссылка на таблицу проектов
    op.create_foreign_key(
        'fk_rule_to_project', 'ProjectRule', 'Project', ['ProjectId'], ['@Project'], ondelete='CASCADE')
    # Ссылка на таблицу проектов
    op.create_foreign_key(
        'fk_rule_to_account', 'ProjectRule', 'Account', ['AccountId'], ['@Account'], ondelete='CASCADE')

    # Индекс с уникальными ролями пользователя в проекте
    op.create_index('unique_account_in_project', 'ProjectRule', ['ProjectId', 'AccountId', 'Rule'], unique=True)


def downgrade():
    op.drop_table('ProjectRule')
