"""Добавлена таблица Image.

Revision ID: b4a631e7c6c7
Revises: c15b03627908
Create Date: 2021-03-16 21:26:09.068967

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b4a631e7c6c7'
down_revision = 'c15b03627908'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'Image',
        sa.Column('@Image', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('Title', sa.String(300), nullable=False),
        sa.Column('Link', sa.TEXT, nullable=False),
        sa.Column('ImageLink', sa.TEXT, nullable=False),
        sa.Column('Description', sa.TEXT),
        sa.Column('UserId', sa.Integer),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now())
    )
    # Ссылка на таблицу пользователей
    op.create_foreign_key('fk_user_id', 'Image', 'User', ['UserId'], ['@User'], ondelete='CASCADE')

    # Ссылка на изображения для проекта
    op.add_column('Project', sa.Column('Image', sa.Integer))
    op.create_foreign_key('fk_image_link', 'Project', 'Image', ['Image'], ['@Image'], ondelete='CASCADE')

    # Ссылка на изображения для аккаунта
    op.add_column('Account', sa.Column('Image', sa.Integer))
    op.create_foreign_key('fk_image_link', 'Account', 'Image', ['Image'], ['@Image'], ondelete='CASCADE')


def downgrade():
    op.drop_constraint('fk_image_link', 'Project')
    op.drop_constraint('fk_image_link', 'Account')
    op.drop_column('Project', 'Image')
    op.drop_column('Account', 'Image')
    op.drop_table('Image')
