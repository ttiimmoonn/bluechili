"""Добавлена таблица Project

Revision ID: 1313db4781b0
Revises: 0e01b1e40805
Create Date: 2021-03-16 01:12:45.511647

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1313db4781b0'
down_revision = '0e01b1e40805'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'Project',
        sa.Column('@Project', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('Name', sa.String(300), nullable=False),
        sa.Column('Description', sa.TEXT, nullable=False),
        sa.Column('Date', sa.DateTime, nullable=False),
        sa.Column('Repository', sa.TEXT, nullable=False),
        sa.Column('LongDescription', sa.TEXT, nullable=False),
        sa.Column('UserId', sa.Integer),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now())
    )

    # Ссылка на таблицу пользователей
    op.create_foreign_key('fk_user_id', 'Project', 'User', ['UserId'], ['@User'], ondelete='CASCADE')


def downgrade():
    op.drop_table('Project')
