"""Добавлена таблица Permission.

Revision ID: 0e01b1e40805
Revises: 35e239b9a53c
Create Date: 2021-03-16 00:59:10.492838

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0e01b1e40805'
down_revision = '35e239b9a53c'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'Permission',
        sa.Column('@Permission', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('UserId', sa.Integer),
        sa.Column('Name', sa.String(300), nullable=False),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now())
    )

    # Ссылка на таблицу пользователей
    op.create_foreign_key('fk_user_id', 'Permission', 'User', ['UserId'], ['@User'], ondelete='CASCADE')

    # Индекс с уникальными правами у пользователей
    op.create_index('unique_permission_to_user', 'Permission', ['UserId', 'Name'], unique=True)


def downgrade():
    op.drop_table('Permission')
