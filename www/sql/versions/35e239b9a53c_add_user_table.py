"""Добавлена таблица User для хранения пользователей.

Revision ID: 35e239b9a53c
Revises: 
Create Date: 2021-03-15 23:01:26.681861

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy.sql import expression


# revision identifiers, used by Alembic.
revision = '35e239b9a53c'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'User',
        sa.Column('@User', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('Login', sa.String(100), nullable=False),
        sa.Column('Password', sa.String(100), nullable=False),
        sa.Column('IsSuperuser', sa.Boolean, nullable=False, server_default=expression.false()),
        sa.Column('Disabled', sa.Boolean, nullable=False, server_default=expression.false()),
        sa.Column('Create', sa.DateTime(timezone=True), server_default=sa.sql.func.now())
    )


def downgrade():
    op.drop_table('User')
