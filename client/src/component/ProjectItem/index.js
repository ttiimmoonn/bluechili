import React, { Component } from 'react'
import { Card, Image, Button } from 'semantic-ui-react'

/**
 * Крточка проекта.
 */
class ProjectItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: this.props.id,
      name: this.props.name,
      image: this.props.image,
      date: this.props.date,
      description: this.props.description,
      repository: this.props.repository
    }
  }

  goToRepository() {
    window.location.assign(this.state.repository);
  }

  render() {  
    return (
      <Card centered style={{overflow: 'auto', width: '100%' }}>
        <Image src={this.state.image} wrapped ui={false} />
        <Card.Content>
          <Card.Header>{this.state.name}</Card.Header>
          <Card.Meta>
            <span className='date'>{this.state.date}</span>
          </Card.Meta>
          <Card.Description>
            {this.state.description}
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
          <Button.Group>
            <Button onClick={() => { this.props.updateDate(this.state.id, true) }
              }>О проекте</Button>
            <Button.Or />
            <Button positive onClick={(e) => this.goToRepository(e)}>Репозиторий</Button>
          </Button.Group>
        </Card.Content>
      </Card>
    )
  }
}

export default ProjectItem