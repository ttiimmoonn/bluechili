import React, { Component } from 'react'
import { Menu, Segment, Visibility, Container, Header, Button, Icon } from 'semantic-ui-react'
import { BrowserRouter as Router, Route, Switch, Link, } from 'react-router-dom'

import MainContent from "../MainContent"
import ModalProject from "../ModalProject"

const route = {
    '/about': "О нас",
    '/participant': "Участники",
    '/project': "Проекты",
    '/': "Главная"
}

const HomepageHeading = ({ mobile }) => (
    <Segment 
        inverted
        textAlign='center'
        vertical
    >
        <Container text>
            <Header
                as='h1'
                content='BlueChili'
                inverted
                style={{
                fontSize: mobile ? '2em' : '10em',
                fontWeight: 'normal',
                marginTop: mobile ? '1.5em' : '1em',
                }}
            />
            <Header
                as='h2'
                content='Web проекты для души и для дела'
                inverted
                style={{
                fontSize: mobile ? '1.5em' : '1.5em',
                fontWeight: 'normal',
                marginTop: mobile ? '0.5em' : '1.5em',
                marginBottom: mobile ? '1.5em' : '7em',
                }}
            />
        </Container>
    </Segment>
  )

class Main extends Component {
    constructor(props) {
        super(props)
        this.api = props.api
        this.state = {
            current_page: route[window.location.pathname],
            menu_fixed: false
        }
    }

    handleItemClick = (e, { name }) => {
        let state = this.state
        state.current_page = name
        this.setState(state)
    }

    hideFixedMenu = () => {
        let state = this.state
        state.menu_fixed = true
        this.setState(state)
    }

    showFixedMenu = () => {
        let state = this.state
        state.menu_fixed = false
        this.setState(state)
    }

    render() {
        return( 
            <Router>
                <Visibility
                    once={false}
                    onBottomPassed={this.showFixedMenu}
                    onBottomPassedReverse={this.hideFixedMenu}
                >
                    <Segment 
                        inverted
                        textAlign='center'
                        style={{ minHeight: 50, padding: '0em 4em' }}
                        vertical
                    >
                        <Menu 
                            fixed={this.state.fixed ? 'top' : null}
                            inverted={!this.state.fixed}
                            pointing={!this.state.fixed}
                            secondary={!this.state.fixed}
                            size='large'
                        >
                            <Link to="/">
                                <Menu.Item
                                    name='Главная'
                                    active={this.state.current_page === 'Главная'}
                                    onClick={this.handleItemClick}
                                />
                            </Link>
                            <Link to="/project">
                                <Menu.Item
                                    name='Проекты'
                                    active={this.state.current_page === 'Проекты'}
                                    onClick={this.handleItemClick}
                                />
                            </Link>
                            <Link to="/participant">
                                <Menu.Item
                                    name='Участники'
                                    active={this.state.current_page === 'Участники'}
                                    onClick={this.handleItemClick}
                                />
                            </Link>
                            <Link to="/about">
                                <Menu.Item
                                    name='О нас'
                                    active={this.state.current_page === 'О нас'}
                                    onClick={this.handleItemClick}
                                />
                            </Link>  
                        </Menu>
                    </Segment>
                </Visibility>
            <Switch>
                <Route path="/project" exact>
                    <MainContent api={this.props.api}/>
                </Route>
                <Route 
                    path="/project/:id" 
                    exact
                    render ={() => <ModalProject api={this.props.api}/>}
                />
                <Route path="/" exact>
                    <HomepageHeading/>
                    <MainContent api={this.props.api}/>
                </Route>
                <Route path="/participant" children={<h3>участники</h3>} />
                <Route path="/about" children={<h3>О нас</h3>} />
            </Switch>
          </Router>
        )
    }
}

export default Main;