import React, { Component } from 'react'
import { Grid, Button, Icon, Visibility, Card, Image } from 'semantic-ui-react'
import ProjectItem from '../ProjectItem'
import ModalProject from '../ModalProject'

const Header = {
  padding: "30px 30px"
}

/**
 * Главная страница с лентой карточек проектов.
 */
 class MainContent extends Component {
  constructor(props) {
    super(props)
    this.api = props.api
    this.state = {
      is_auth: false,
      current_id: null,
      modal_open: false,
      projectListData: []
    }
  }

  componentDidMount() {
    let state = this.state;
    this.api.Status()
    .then(this.api.checkStatus)
    .then((result) => {
      state.is_auth = true
    })
    .catch((error) => {})
    .then(() => {
      this.api.GetProjectListWithImage({'Limit': null, 'Page': null})
      .then(this.api.checkStatus)
      .then((result) => {
          console.log(result)
          state.projectListData = result;
          this.setState(state)
      })
      .catch((error) => {
          console.log(error)
      })
    })
  }

  getProjectList = () => {
    let state = this.state
    this.api.GetProjectListWithImage({'Limit': null, 'Page': null})
    .then(this.api.checkStatus)
    .then((result) => {
        console.log(result)
        state.projectListData = result;
        this.setState(state)
    })
    .catch((error) => {
        console.log(error)
    })
  }

  // Обновляем модальное окно с проектом.
  updateModal = (id, modal_state) => {
    this.setState({ current_id: id, modal_open: modal_state })
  }

  deleteProject = (project_id) => {
    console.log(project_id)
    this.api.DeleteProject({Id: project_id})
    .then(this.api.checkStatus)
    .then((result) => {
      console.log(`Удалили проект ${project_id}`)
      this.getProjectList()
    })
    .catch((error) => {
      console.log(error)
  })
  }

  render() {
    return (
      <Card.Group style={Header} itemsPerRow={2}>
        {
          this.state.projectListData.map(
            (project) => 
            (
              <Card>
                <Image src={project.ImageLink} wrapped ui={false} as='a' size='medium' href={'/project/' + project.Id} target='_blank' />
                <Card.Content>
                  <Card.Header>{project.Name}</Card.Header>
                  <Card.Meta>{project.Date}</Card.Meta>
                  <Card.Description>
                    {project.Description}
                  </Card.Description>
                </Card.Content>
                { this.state.is_auth ?
                  <Button.Group>
                    <Button href={'/admin/project/' + project.Id} icon>
                        <Icon name='align edit' />
                      </Button>
                    <Button icon onClick={() => this.deleteProject(project.Id)}>
                      <Icon name='align delete' />
                    </Button>
                  </Button.Group> : 
                  <div></div>
                }
              </Card>
            )
          )
        }
      </Card.Group>
    )
  }
}

export default MainContent