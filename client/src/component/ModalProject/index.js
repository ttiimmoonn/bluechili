import React, { Component } from 'react'
import { Container, Segment, Image, Header, Button } from 'semantic-ui-react'
import CastomEditor from '../Editor'
import ReadOnlyEditor from '../Editor'


/**
 * Модальное окно проекта.
 */
class ModalProject extends Component {
  constructor(props) {
    super(props)
    this.prject_id = parseInt(window.location.pathname.split("/")[2])
    this.api = props.api

    this.state = {
      project: {}
    }
  }

  componentDidMount() {
    this.api.GetProject({'Id': this.prject_id})
    .then(this.api.checkStatus)
    .then((result) => {
        console.log(result)
        let state = this.state;
        state.project = result;
        this.setState (state)
    })
    .catch((error) => {
        console.log(error)
    })
  }
  render() {  
    return (
      <Segment 
        inverted
        vertical
      >
          <Container text textAlign='center'>
              <Header
                  as='h1'
                  content={this.state.project.Name}
                  inverted
                  style={{
                  fontSize: '10em',
                  fontWeight: 'normal',
                  marginTop: '1em',
                  }}
              />
              <Header
                  as='h2'
                  content={this.state.project.Description}
                  inverted
                  style={{
                  fontSize: '1.5em',
                  fontWeight: 'normal',
                  marginTop: '1.5em',
                  marginBottom:'4em',
                  }}
              />
          </Container>
          <Button circular href={this.state.Repository} textAlign='right' style={{margin: "3em"}} color='gitlab' icon='gitlab' />
      </Segment>
    )
  }

}

export default ModalProject