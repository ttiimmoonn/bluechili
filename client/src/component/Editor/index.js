import React from 'react';
import ReactDOM from 'react-dom';
import {Editor, EditorState, ConvertFromRaw, convertToRaw, RichUtils} from 'draft-js';
import 'draft-js/dist/Draft.css';


class CastomEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {editorState: EditorState.createEmpty()};
    this.onChange = editorState => this.setState({editorState});
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
  }

  handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);

    if (newState) {
      this.onChange(newState);
      return 'handled';
    }

    return 'not-handled';
  }

  logDate() {
    console.log(JSON.stringify(convertToRaw(this.state.editorState)))
  }

  render() {
    return (
      <div>
        <Editor
          editorState={this.state.editorState}
          handleKeyCommand={this.handleKeyCommand}
          onChange={this.onChange}
        />
        <button onClick={this.logDate}>кнопка</button>
      </div>
    );
  }
}

const ReadOnlyEditor = (props) => {
  const storedState = ConvertFromRaw(JSON.parse(props.storedState));
  return (
     <div className="readonly-editor">
       <Editor editorState={storedState} readOnly={true} /> 
     </div>
  );
}

export default { CastomEditor, ReadOnlyEditor }