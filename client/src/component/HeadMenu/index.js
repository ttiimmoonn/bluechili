import React, { Component } from 'react'
import { Menu, Segment } from 'semantic-ui-react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'


class HeadMenu extends Component {
    state = { activeItem: 'Проекты' }
  
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  
    render() {
      const { activeItem } = this.state
  
      return (
        <Segment inverted>
          <Switch>
            <Menu inverted secondary>
              <Route path="/project">
                <Menu.Item
                  name='Проекты'
                  active={activeItem === 'Проекты'}
                  onClick={this.handleItemClick}
                />
              </Route>
              <Menu.Item
                name='Участники'
                active={activeItem === 'Участники'}
                onClick={this.handleItemClick}
              />
              <Menu.Item
                name='О нас'
                active={activeItem === 'О нас'}
                onClick={this.handleItemClick}
              />
            </Menu>
          </Switch>
        </Segment>
      )
    }
  }

export default HeadMenu