import React from 'react';

import { Button, Message, Form, Segment, Grid, Header } from 'semantic-ui-react'

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.api = props.api
        this.state = {
            is_valid: true,
            login: "",
            password: ""
        }
    }

    componentDidMount() {
        this.api.Status()
        .then(this.api.checkStatus)
        .then((result) => {
            document.location.href = '/';
        })
        .catch((error) => {
            console.log("Необходима авторизация")
        })
      }

    handleChange = (e, { name, value }) => {
        if ( !this.state.is_valid) {
            let state = this.state;
            state.is_valid = true;
            this.setState(state)
        }

        let state = this.state;
        state[name] = value;
        this.setState(state)
    }

    handleSubmit = () => {
        const {login, password} = this.state;
        this.api.Login({"Login": login, "Password": password})
        .then(this.api.checkStatus)
        .then((result) => {
            document.location.href = '/';
        })
        .catch((error) => {
            console.log(error)
            let state = this.state;
            state.is_valid = false;
            this.setState(state)
        })
    }

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Form error={!this.state.is_valid} onSubmit={this.handleSubmit} inverted size='large'>
                        <Segment inverted stacked>
                            <Header textAlign='center' as='h1'>Авторизация</Header>
                            <Form.Input
                                icon='user'
                                label='Логин'
                                name='login'
                                value={this.state.login}
                                onChange={this.handleChange}
                            />
                            <Form.Input
                                icon='lock'
                                label='Пароль'
                                name='password'
                                type='password'
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                            <Message
                                error
                                header='Ошибка авторизации'
                                content='Проверьте правильность данных и повторите попытку снова.'
                            />
                            <Button type='submit'>Отправить</Button>
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        )
    } 
}

export default Auth;