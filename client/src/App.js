import 'semantic-ui-css/semantic.min.css'

import Api from './Api'

import React from 'react'

import Auth from './component/Admin/auth'
import Main from './component/Main'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// import ProjectItem from './component/Admin/project/ProjectItem'


/**
 * Основной компонент приложения.
 */
 export default class App extends React.Component{
  constructor(props) {
    super(props)
    this.api = new Api({http: true, ip: '127.0.0.1', port: '8080'})
  }
  render() {
    return (
      <Router>
        <Route path="/admin/login" render={() => <Auth api = {this.api}/>}/>
        {/* <Route path="/admin/project/:id" render={() => <ProjectItem api = {this.api}/>}/> */}
        <Route path="/" render={() => <Main api = {this.api}/>}/>
      </Router>
    );
  }
}

{/* <Route exact path="/" component={Main} />
      <Route path="/admin/login" render={() => <Auth api = {api}/>}/>
    <Router>
    <HeadMenu />
    <MainContent api = {api} />
    </Router> */}