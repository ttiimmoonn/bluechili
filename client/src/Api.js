class Connect {
    constructor(props) {

        this.ip = props.ip
        this.port = props.port

        if (props.http) {
            this.protocol = 'http'
        } else {
            this.protocol = 'https'
        }

        if (props.version) {
            this.version = props.version
        } else {
            this.version = 'v1'
        }

        if (props.headers) {
            this.headers = props.headers
        } else {
            this.headers = {}
        }

        this.mode = 'cors'
        this.credentials = 'include';

    }

    getSetverUrl = () => {
        return `${this.protocol}://${this.ip}:${this.port}/api/${this.version}`
    }

    checkStatus = (response) => {
        console.log(response)
        const contentType = response.headers.get("content-type");
        if (response.status >= 200 && response.status < 300) {
            if (contentType && contentType.indexOf("application/json") !== -1) {
                console.log('Есть тело и успех')
                return response.json();
            } else if (contentType && contentType.indexOf("text/plain") !== -1) {
                console.log('Есть тело и успех, ')
                return response.text();
            } else {
                console.log('Нет тела и успех')
                return;
            }
        } else {
            if (contentType && contentType.indexOf("application/json") !== -1) {
                console.log('Есть тело и fail | снаруже')
                return response.json()
                    .then((error) => {
                        console.log('Есть тело и fail | внутри', error)
                        throw new Error(error.message)
                    })
            } else {
                console.log('Нет тела и fail')
                throw new Error({ message: '' });
            }
        }
    }
}

class Api extends Connect {
    constructor(...args) {
        super(...args);
    }

    // Получить список проектов
    GetProjectListWithImage = (props) => {
        let url = `${this.getSetverUrl()}/registry/get_project_list_with_image`
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props)
        })
    }   
    
    // Авторизация
    Login = (props) => {
        let url = `${this.getSetverUrl()}/auth/login`
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
        console.log(this.credentials)
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props)
        })
    }    

    // Получить идентификатор текущего пользователя по сессии
    Status = (props) => {
        let url = `${this.getSetverUrl()}/auth/status`
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
        console.log(this.credentials)
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    }    

    // Получить информацию по проекту
    GetProject = (props) => {
        let url = `${this.getSetverUrl()}/project/get`
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props)
        })
    } 
    
    // Удалить проект
    DeleteProject = (props) => {
        let url = `${this.getSetverUrl()}/project/delete`
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props)
        })
    }   
}

export default Api